package com.benjamin.parsy.springbatchhelper.test.job.filmap;

import com.benjamin.parsy.springbatchhelper.test.entity.AuthorEntity;
import com.benjamin.parsy.springbatchhelper.test.repository.AuthorRepository;
import org.springframework.batch.item.data.RepositoryItemReader;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component(value = FilmapJobConfig.STEP_READER_NAME)
class FilmapReader extends RepositoryItemReader<AuthorEntity> {

    public FilmapReader(AuthorRepository authorRepository) {
        super();
        setRepository(authorRepository);
        setMethodName("findAll");
        setSort(Map.of("id", Sort.Direction.ASC));
    }

}
