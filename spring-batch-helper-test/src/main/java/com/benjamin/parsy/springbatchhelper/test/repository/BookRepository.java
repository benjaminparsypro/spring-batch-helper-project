package com.benjamin.parsy.springbatchhelper.test.repository;

import com.benjamin.parsy.springbatchhelper.test.entity.BookEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BookRepository extends JpaRepository<BookEntity, Long> {
}
