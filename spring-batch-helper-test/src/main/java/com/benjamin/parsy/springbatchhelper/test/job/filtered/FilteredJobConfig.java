package com.benjamin.parsy.springbatchhelper.test.job.filtered;

import com.benjamin.parsy.springbatchhelper.test.entity.AuthorEntity;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@EnableBatchProcessing
@Configuration
class FilteredJobConfig {

    private static final String NAME = "filtered";

    public static final String JOB_NAME = NAME + "Job";
    public static final String STEP_NAME = NAME + "ChunkStep";
    public static final String STEP_READER_NAME = STEP_NAME + "Reader";
    public static final String STEP_PROC_NAME = STEP_NAME + "Proc";
    public static final String STEP_WRITER_NAME = STEP_NAME + "Writer";

    public static final String OUTPUT_DIR_PARAM = "outputDir";
    public static final String RUN_ID_PARAM = "run.id";

    @Bean(JOB_NAME)
    public Job job(JobBuilderFactory jobBuilderFactory,
                   @Qualifier(STEP_NAME) Step chunkStep) {

        return jobBuilderFactory.get(JOB_NAME)
                .start(chunkStep)
                .build();
    }

    @Bean(STEP_NAME)
    public Step step(StepBuilderFactory stepBuilderFactory,
                     @Qualifier(STEP_READER_NAME) ItemReader<AuthorEntity> reader,
                     @Qualifier(STEP_PROC_NAME) ItemProcessor<AuthorEntity, ItemWrite> proc,
                     @Qualifier(STEP_WRITER_NAME) ItemWriter<ItemWrite> writer) {

        return stepBuilderFactory.get(STEP_NAME)
                .<AuthorEntity, ItemWrite>chunk(100)
                .reader(reader)
                .processor(proc)
                .writer(writer)
                .build();
    }

}
