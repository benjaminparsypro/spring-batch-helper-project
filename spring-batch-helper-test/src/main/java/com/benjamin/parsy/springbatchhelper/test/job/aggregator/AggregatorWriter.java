package com.benjamin.parsy.springbatchhelper.test.job.aggregator;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.apache.commons.lang3.StringUtils;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.core.listener.StepExecutionListenerSupport;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.item.ItemStreamException;
import org.springframework.batch.item.ItemStreamWriter;
import org.springframework.batch.item.json.JacksonJsonObjectMarshaller;
import org.springframework.batch.item.json.JsonFileItemWriter;
import org.springframework.batch.item.json.builder.JsonFileItemWriterBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.FileSystemResource;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Component;

import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.util.List;
import java.util.Map;

@Component(value = AggregatorJobConfig.STEP_WRITER_NAME)
@StepScope
class AggregatorWriter extends StepExecutionListenerSupport implements ItemStreamWriter<ItemWrite> {

    private final JsonFileItemWriter<ItemWrite> delegate;

    public AggregatorWriter(@Value("#{jobParameters}") Map<String, Object> jobParameters) {
        String outputDir = (String) jobParameters.getOrDefault(AggregatorJobConfig.OUTPUT_DIR_PARAM, StringUtils.EMPTY);
        this.delegate = createDelegate(outputDir);
    }

    private JsonFileItemWriter<ItemWrite> createDelegate(String outputDir) {

        String filename = String.format("file_%d.json", System.currentTimeMillis());
        Path outputFile = Path.of(outputDir, filename);

        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.writerWithDefaultPrettyPrinter();
        objectMapper.enable(SerializationFeature.INDENT_OUTPUT);

        return new JsonFileItemWriterBuilder<ItemWrite>()
                .name(AggregatorWriter.class.getName())
                .jsonObjectMarshaller(new JacksonJsonObjectMarshaller<>(objectMapper))
                .resource(new FileSystemResource(outputFile))
                .encoding(StandardCharsets.UTF_8.name())
                .build();
    }

    @Override
    public void open(@NonNull ExecutionContext executionContext) throws ItemStreamException {
        delegate.open(executionContext);
    }

    @Override
    public void update(@NonNull ExecutionContext executionContext) throws ItemStreamException {
        delegate.update(executionContext);
    }

    @Override
    public void close() throws ItemStreamException {
        delegate.close();
    }

    @Override
    public void write(@NonNull List<? extends ItemWrite> list) throws Exception {
        delegate.write(list);
    }
}
