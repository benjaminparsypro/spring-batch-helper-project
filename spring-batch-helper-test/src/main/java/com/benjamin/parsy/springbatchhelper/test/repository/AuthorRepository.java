package com.benjamin.parsy.springbatchhelper.test.repository;

import com.benjamin.parsy.springbatchhelper.test.entity.AuthorEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AuthorRepository extends JpaRepository<AuthorEntity, Long> {
}
