package com.benjamin.parsy.springbatchhelper.test.job.mapped;

import com.benjamin.parsy.springbatchhelper.test.entity.AuthorEntity;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.support.CompositeItemWriter;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.List;

@EnableBatchProcessing
@Configuration
class MappedJobConfig {

    private static final String NAME = "mapped";

    public static final String JOB_NAME = NAME + "Job";
    public static final String STEP_NAME = NAME + "ChunkStep";
    public static final String STEP_READER_NAME = STEP_NAME + "Reader";
    public static final String STEP_PROC_NAME = STEP_NAME + "Proc";
    public static final String STEP_WRITER_NAME = STEP_NAME + "Writer";
    public static final String STEP_AUTHOR_WRITER_NAME = STEP_NAME + "AuthorWriter";
    public static final String STEP_BOOK_WRITER_NAME = STEP_NAME + "BookWriter";

    public static final String OUTPUT_DIR_PARAM = "outputDir";
    public static final String RUN_ID_PARAM = "run.id";

    @Bean(JOB_NAME)
    public Job job(JobBuilderFactory jobBuilderFactory,
                   @Qualifier(STEP_NAME) Step chunkStep) {

        return jobBuilderFactory.get(JOB_NAME)
                .start(chunkStep)
                .build();
    }

    @Bean(STEP_NAME)
    public Step step(StepBuilderFactory stepBuilderFactory,
                     @Qualifier(STEP_READER_NAME) ItemReader<AuthorEntity> reader,
                     @Qualifier(STEP_PROC_NAME) ItemProcessor<AuthorEntity, ItemWrite> proc,
                     @Qualifier(STEP_WRITER_NAME) CompositeItemWriter<ItemWrite> compositeItemWriter) {

        return stepBuilderFactory.get(STEP_NAME)
                .<AuthorEntity, ItemWrite>chunk(100)
                .reader(reader)
                .processor(proc)
                .writer(compositeItemWriter)
                .build();
    }

    @Bean(STEP_WRITER_NAME)
    public CompositeItemWriter<ItemWrite> compositeItemWriter(@Qualifier(STEP_AUTHOR_WRITER_NAME) ItemWriter<ItemWrite> authorWriter,
                                                              @Qualifier(STEP_BOOK_WRITER_NAME) ItemWriter<ItemWrite> bookWriter) {

        CompositeItemWriter<ItemWrite> compositeItemWriter = new CompositeItemWriter<>();
        compositeItemWriter.setDelegates(List.of(authorWriter, bookWriter));

        return compositeItemWriter;
    }

}
