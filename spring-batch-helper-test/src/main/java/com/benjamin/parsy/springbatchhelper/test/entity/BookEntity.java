package com.benjamin.parsy.springbatchhelper.test.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;

@EqualsAndHashCode(callSuper = true)
@Data
@Entity
@Table(name = "book")
public class BookEntity extends BaseEntity {

    @Column(name = "title")
    private String title;

    @Column(name = "page")
    private int page;

    @Column(name = "price")
    private float price;

    @JoinColumn(name = "author_id")
    @ManyToOne
    private AuthorEntity author;

}
