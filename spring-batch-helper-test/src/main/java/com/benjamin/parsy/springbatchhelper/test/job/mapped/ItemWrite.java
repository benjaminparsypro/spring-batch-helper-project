package com.benjamin.parsy.springbatchhelper.test.job.mapped;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
class ItemWrite {

    @JsonProperty
    private String firstname;

    @JsonProperty
    private String lastname;

    @JsonProperty
    private List<Book> books;

    @Getter
    @Setter
    @NoArgsConstructor
    @AllArgsConstructor
    @JsonRootName(value = "book")
    public static class Book {

        @JsonProperty
        private String name;

    }

}
