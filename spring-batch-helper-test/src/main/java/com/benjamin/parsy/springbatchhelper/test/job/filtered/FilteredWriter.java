package com.benjamin.parsy.springbatchhelper.test.job.filtered;

import com.benjamin.parsy.springbatchhelper.FilteredItemWriter;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.apache.commons.lang3.StringUtils;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.item.json.JacksonJsonObjectMarshaller;
import org.springframework.batch.item.json.JsonFileItemWriter;
import org.springframework.batch.item.json.builder.JsonFileItemWriterBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.FileSystemResource;
import org.springframework.stereotype.Component;

import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.util.Map;
import java.util.function.Predicate;

@Component(value = FilteredJobConfig.STEP_WRITER_NAME)
@StepScope
class FilteredWriter extends FilteredItemWriter<ItemWrite> {

    public FilteredWriter(@Value("#{jobParameters}") Map<String, Object> jobParameters) {
        super(createDelegate(jobParameters), createPredicate());
    }

    private static JsonFileItemWriter<ItemWrite> createDelegate(Map<String, Object> jobParameters) {

        String outputDir = (String) jobParameters.getOrDefault(FilteredJobConfig.OUTPUT_DIR_PARAM, StringUtils.EMPTY);

        String filename = String.format("file_%d.json", System.currentTimeMillis());
        Path outputFile = Path.of(outputDir, filename);

        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.writerWithDefaultPrettyPrinter();
        objectMapper.enable(SerializationFeature.INDENT_OUTPUT);

        return new JsonFileItemWriterBuilder<ItemWrite>()
                .name(FilteredWriter.class.getName())
                .jsonObjectMarshaller(new JacksonJsonObjectMarshaller<>(objectMapper))
                .resource(new FileSystemResource(outputFile))
                .encoding(StandardCharsets.UTF_8.name())
                .build();
    }

    private static Predicate<ItemWrite> createPredicate() {
        return itemWrite -> !itemWrite.getBooks().isEmpty();
    }

}
