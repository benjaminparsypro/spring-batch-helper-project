package com.benjamin.parsy.springbatchhelper.test.job.filtered;

import com.benjamin.parsy.springbatchhelper.test.entity.AuthorEntity;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component(value = FilteredJobConfig.STEP_PROC_NAME)
class FilteredProc implements ItemProcessor<AuthorEntity, ItemWrite> {

    @Override
    public ItemWrite process(AuthorEntity authorEntity) {

        ItemWrite itemWrite = new ItemWrite();
        itemWrite.setFirstname(authorEntity.getFirstname());
        itemWrite.setLastname(authorEntity.getLastname());

        List<ItemWrite.Book> bookList = authorEntity.getBooks().stream()
                .map(b -> new ItemWrite.Book(b.getTitle()))
                .collect(Collectors.toList());

        itemWrite.setBooks(bookList);

        return itemWrite;
    }

}
