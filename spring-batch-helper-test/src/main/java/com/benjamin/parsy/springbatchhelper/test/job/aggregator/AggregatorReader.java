package com.benjamin.parsy.springbatchhelper.test.job.aggregator;

import com.benjamin.parsy.springbatchhelper.AggregatorCompletor;
import com.benjamin.parsy.springbatchhelper.AggregatorItemReader;
import com.benjamin.parsy.springbatchhelper.AggregatorReducer;
import com.benjamin.parsy.springbatchhelper.test.entity.AuthorEntity;
import com.benjamin.parsy.springbatchhelper.test.entity.BookEntity;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.database.builder.JdbcCursorItemReaderBuilder;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

@Component(value = AggregatorJobConfig.STEP_READER_NAME)
class AggregatorReader extends AggregatorItemReader<ItemRawRead, AuthorEntity> {

    private static final String SQL =
            "select a.id            as authorId, " +
                "a.firstname        as authorFirstname, " +
                "a.lastname         as authorLastname, " +
                "b.id               as bookId, " +
                "b.title            as bookTitle, " +
                "b.page             as bookPage, " +
                "b.price            as bookPrice " +
            "from author a " +
                "left join book b on a.id = b.author_id " +
            "order by a.firstname";

    protected AggregatorReader(DataSource dataSource) {
        super(delegate(dataSource), aggregatorCompletor(), aggregatorReducer());
    }

    private static ItemReader<ItemRawRead> delegate(DataSource dataSource) {

        return new JdbcCursorItemReaderBuilder<ItemRawRead>()
                .name(AggregatorReader.class.getName())
                .rowMapper(new BeanPropertyRowMapper<>(ItemRawRead.class))
                .sql(SQL)
                .dataSource(dataSource)
                .build();
    }

    private static AggregatorCompletor<ItemRawRead> aggregatorCompletor() {
        return (lastRawItem, currentRawItem) -> lastRawItem.getAuthorId().equals(currentRawItem.getAuthorId());
    }

    private static AggregatorReducer<ItemRawRead, AuthorEntity> aggregatorReducer() {
        return rawItemList -> {

            ItemRawRead commonInformations = rawItemList.get(0);

            AuthorEntity authorEntity = new AuthorEntity();
            authorEntity.setId(commonInformations.getAuthorId());
            authorEntity.setFirstname(commonInformations.getAuthorFirstname());
            authorEntity.setLastname(commonInformations.getAuthorLastname());

            Set<BookEntity> bookEntitySet = rawItemList.stream()
                    .map(i -> createBookEntity(i, authorEntity))
                    .filter(Objects::nonNull)
                    .collect(Collectors.toSet());

            authorEntity.setBooks(bookEntitySet);

            return authorEntity;
        };
    }

    private static BookEntity createBookEntity(ItemRawRead itemRawRead, AuthorEntity authorEntity) {

        if (itemRawRead.getBookId() == null) {
            return null;
        }

        BookEntity bookEntity = new BookEntity();
        bookEntity.setId(itemRawRead.getBookId());
        bookEntity.setPage(itemRawRead.getBookPage());
        bookEntity.setTitle(itemRawRead.getBookTitle());
        bookEntity.setPrice(itemRawRead.getBookPrice());
        bookEntity.setAuthor(authorEntity);

        return bookEntity;
    }

}
