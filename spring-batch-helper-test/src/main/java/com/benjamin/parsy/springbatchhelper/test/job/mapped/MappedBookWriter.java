package com.benjamin.parsy.springbatchhelper.test.job.mapped;

import com.benjamin.parsy.springbatchhelper.FlatMapper;
import com.benjamin.parsy.springbatchhelper.MappedItemWriter;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.apache.commons.lang3.StringUtils;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.item.json.JacksonJsonObjectMarshaller;
import org.springframework.batch.item.json.JsonFileItemWriter;
import org.springframework.batch.item.json.builder.JsonFileItemWriterBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.FileSystemResource;
import org.springframework.stereotype.Component;

import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.util.Map;
import java.util.stream.Stream;

@Component(value = MappedJobConfig.STEP_BOOK_WRITER_NAME)
@StepScope
class MappedBookWriter extends MappedItemWriter<ItemWrite, ItemWrite.Book> {

    public MappedBookWriter(@Value("#{jobParameters}") Map<String, Object> jobParameters) {
        super(createDelegate(jobParameters), createFlatmapper());
    }

    private static JsonFileItemWriter<ItemWrite.Book> createDelegate(Map<String, Object> jobParameters) {

        String outputDir = (String) jobParameters.getOrDefault(MappedJobConfig.OUTPUT_DIR_PARAM, StringUtils.EMPTY);

        String filename = String.format("book_%d.json", System.currentTimeMillis());
        Path outputFile = Path.of(outputDir, filename);

        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.writerWithDefaultPrettyPrinter();
        objectMapper.enable(SerializationFeature.INDENT_OUTPUT);

        return new JsonFileItemWriterBuilder<ItemWrite.Book>()
                .name(MappedBookWriter.class.getName())
                .jsonObjectMarshaller(new JacksonJsonObjectMarshaller<>(objectMapper))
                .resource(new FileSystemResource(outputFile))
                .encoding(StandardCharsets.UTF_8.name())
                .build();
    }

    private static FlatMapper<ItemWrite, Stream<ItemWrite.Book>> createFlatmapper() {
        return itemWrite -> itemWrite.getBooks().stream();
    }

}
