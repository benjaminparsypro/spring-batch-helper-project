package com.benjamin.parsy.springbatchhelper.test.job.aggregator;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
class ItemRawRead {

    private Long authorId;
    private String authorFirstname;
    private String authorLastname;
    private Long bookId;
    private String bookTitle;
    private Integer bookPage;
    private Float bookPrice;

}
