package com.benjamin.parsy.springbatchhelper.test;

import org.springframework.batch.test.JobScopeTestExecutionListener;
import org.springframework.batch.test.StepScopeTestExecutionListener;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestExecutionListeners;

@SpringBootTest(classes = AbstractBatchTest.TestAppConfiguration.class)
@ActiveProfiles("test")
public abstract class AbstractBatchTest {

    @Configuration
    @EnableAutoConfiguration
    @Import(TestConfiguration.class)
    @TestExecutionListeners(listeners = {
            StepScopeTestExecutionListener.class,
            JobScopeTestExecutionListener.class
    })
    public static class TestAppConfiguration {}

}
