package com.benjamin.parsy.springbatchhelper.test;

import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.fail;

public class TestUtils {

    private TestUtils() {
        // Private constructor for utility class
    }

    public static List<Path> listPath(Path outputDir) {

        try (Stream<Path> pathStream = Files.list(outputDir)) {
            return pathStream.collect(Collectors.toList());
        } catch (Exception e) {
            return fail(e);
        }

    }

    public static Optional<Path> findFirstPath(Path outputDir) {

        try (Stream<Path> pathStream = Files.list(outputDir)) {
            return pathStream.findFirst();
        } catch (Exception e) {
            return fail(e);
        }

    }

}
