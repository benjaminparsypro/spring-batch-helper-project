package com.benjamin.parsy.springbatchhelper.test.job.filtered;

import com.benjamin.parsy.springbatchhelper.test.AbstractBatchTest;
import com.benjamin.parsy.springbatchhelper.test.TestUtils;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.repository.JobRepository;
import org.springframework.batch.test.JobLauncherTestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.util.FileSystemUtils;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;

class FilteredJobTest extends AbstractBatchTest {

    @Autowired
    @Qualifier(FilteredJobConfig.JOB_NAME)
    private Job job;

    @Autowired
    private JobRepository jobRepository;

    @Autowired
    private JobLauncher jobLauncher;

    @TempDir
    static Path tempDir;

    private Path outputTempDir;
    private JobLauncherTestUtils jobLauncherTestUtils;

    @BeforeEach
    void setUp() throws IOException {

        jobLauncherTestUtils = new JobLauncherTestUtils();
        jobLauncherTestUtils.setJob(job);
        jobLauncherTestUtils.setJobRepository(jobRepository);
        jobLauncherTestUtils.setJobLauncher(jobLauncher);

        outputTempDir = Files.createDirectory(Path.of(tempDir.toString(), "output"));
    }

    @AfterEach
    void tearDown() throws IOException {
        FileSystemUtils.deleteRecursively(outputTempDir);
    }

    @Test
    void shouldFilteredItemInWriteFile() throws Exception {

        // Given
        JobParametersBuilder jobParametersBuilder = new JobParametersBuilder();
        jobParametersBuilder.addString(FilteredJobConfig.OUTPUT_DIR_PARAM, outputTempDir.toString());
        jobParametersBuilder.addString(FilteredJobConfig.RUN_ID_PARAM, String.valueOf(System.currentTimeMillis()));

        // When
        JobExecution jobExecution = jobLauncherTestUtils.launchJob(jobParametersBuilder.toJobParameters());

        // Then
        assertEquals(ExitStatus.COMPLETED, jobExecution.getExitStatus());

        Optional<Path> outputOptionalPath = TestUtils.findFirstPath(outputTempDir);
        assertTrue(outputOptionalPath.isPresent());

        ObjectMapper mapper = new ObjectMapper();
        List<ItemWrite> itemWriteList = mapper.readValue(outputOptionalPath.get().toFile(), new TypeReference<>() {});

        assertEquals(4, itemWriteList.size());

        itemWriteList.forEach(i -> assertFalse(i.getBooks().isEmpty()));

        List<ItemWrite.Book> allBooks = itemWriteList.stream()
                .map(ItemWrite::getBooks)
                .flatMap(Collection::stream)
                .collect(Collectors.toList());

        assertEquals(7, allBooks.size());
    }

}
