package com.benjamin.parsy.springbatchhelper.test.job.filmap;

import com.benjamin.parsy.springbatchhelper.test.AbstractBatchTest;
import com.benjamin.parsy.springbatchhelper.test.TestUtils;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.repository.JobRepository;
import org.springframework.batch.test.JobLauncherTestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.util.FileSystemUtils;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class FilmapJobTest extends AbstractBatchTest {

    @Autowired
    @Qualifier(FilmapJobConfig.JOB_NAME)
    private Job job;

    @Autowired
    private JobRepository jobRepository;

    @Autowired
    private JobLauncher jobLauncher;

    @TempDir
    static Path tempDir;

    private Path outputTempDir;
    private JobLauncherTestUtils jobLauncherTestUtils;

    @BeforeEach
    void setUp() throws IOException {

        jobLauncherTestUtils = new JobLauncherTestUtils();
        jobLauncherTestUtils.setJob(job);
        jobLauncherTestUtils.setJobRepository(jobRepository);
        jobLauncherTestUtils.setJobLauncher(jobLauncher);

        outputTempDir = Files.createDirectory(Path.of(tempDir.toString(), "output"));
    }

    @AfterEach
    void tearDown() throws IOException {
        FileSystemUtils.deleteRecursively(outputTempDir);
    }

    @Test
    void shouldFilmapItemInWriteFile() throws Exception {

        // Given
        JobParametersBuilder jobParametersBuilder = new JobParametersBuilder();
        jobParametersBuilder.addString(FilmapJobConfig.OUTPUT_DIR_PARAM, outputTempDir.toString());
        jobParametersBuilder.addString(FilmapJobConfig.RUN_ID_PARAM, String.valueOf(System.currentTimeMillis()));

        // When
        JobExecution jobExecution = jobLauncherTestUtils.launchJob(jobParametersBuilder.toJobParameters());

        // Then
        assertEquals(ExitStatus.COMPLETED, jobExecution.getExitStatus());

        List<Path> outputFilePathList = TestUtils.listPath(outputTempDir);
        assertEquals(2, outputFilePathList.size());

        // Check author file
        Optional<Path> authorOptionalPath = outputFilePathList.stream()
                .filter(o -> o.getFileName().toString().contains("author"))
                .findFirst();

        assertTrue(authorOptionalPath.isPresent());

        List<String> authorList = new ObjectMapper().readValue(authorOptionalPath.get().toFile(), new TypeReference<>() {});

        assertEquals(2, authorList.size());
        assertTrue(authorList.containsAll(List.of("Antoine", "Alexandre")));

        // Check book file
        Optional<Path> bookOptionalPath = outputFilePathList.stream()
                .filter(o -> o.getFileName().toString().contains("book"))
                .findFirst();

        assertTrue(bookOptionalPath.isPresent());

        List<ItemWrite.Book> bookList = new ObjectMapper().readValue(bookOptionalPath.get().toFile(), new TypeReference<>() {});

        assertEquals(3, bookList.size());
    }

}
