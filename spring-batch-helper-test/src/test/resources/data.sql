
insert into public.author (id, firstname, lastname)
values  (1, 'Victor', 'Hugo'),
        (2, 'Antoine', 'de Saint-Exupéry'),
        (3, 'John Ronald Reuel', 'Tolkien'),
        (4, 'Alexandre', 'Dumas'),
        (5, 'Benjamin', 'Parsy');

insert into public.book (id, title, page, price, author_id)
values  (1, 'Le seigneur des anneaux, tome 3 : Le retour du roi', 576, 9.7, 3),
        (2, 'Le Comte de Monte Cristo - : Le Comte de Monte-Cristo', 1264, 12.9, 4),
        (3, 'Les Misérables', 955, 6.9, 1),
        (4, 'Le seigneur des anneaux, tome 1 : La communauté de l''anneau', 704, 9.2, 3),
        (5, 'Le petit prince', 97, 7.5, 2),
        (6, 'Le seigneur des anneaux, tome 2 : Les deux tours', 576, 9.2, 3),
        (7, 'Les Trois Mousquetaires', 888, 5.9, 4);