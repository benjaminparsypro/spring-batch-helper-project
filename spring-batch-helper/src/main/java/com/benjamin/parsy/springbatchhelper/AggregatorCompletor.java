package com.benjamin.parsy.springbatchhelper;

import org.springframework.lang.NonNull;

@FunctionalInterface
public interface AggregatorCompletor<R> {

    /**
     * Function to identify whether the current item should be aggregated with previous items.
     *
     * @param lastRawItem Last raw item read
     * @param currentRawItem Current raw item read
     * @return True if the current item must be aggregated with previous items, otherwise false
     */
    boolean toBeAggregated(@NonNull R lastRawItem, @NonNull R currentRawItem);

}
