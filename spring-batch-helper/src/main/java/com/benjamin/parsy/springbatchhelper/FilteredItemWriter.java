package com.benjamin.parsy.springbatchhelper;

import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.item.ItemStreamException;
import org.springframework.batch.item.ItemStreamWriter;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.lang.NonNull;
import org.springframework.util.Assert;

import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public abstract class FilteredItemWriter<I> implements ItemStreamWriter<I>, InitializingBean {

    private final ItemWriter<I> itemWriterDelegate;
    private final Predicate<I> predicate;

    protected FilteredItemWriter(ItemWriter<I> itemWriterDelegate, Predicate<I> predicate) {
        this.itemWriterDelegate = itemWriterDelegate;
        this.predicate = predicate;
    }

    @Override
    public void afterPropertiesSet() {
        Assert.notNull(this.predicate, "Predicate is required");
        Assert.notNull(this.itemWriterDelegate, "ItemWriterDelegate is required");
    }

    @Override
    public void write(@NonNull List<? extends I> list) throws Exception {
        List<I> collected = list.stream().filter(predicate).collect(Collectors.toList());
        itemWriterDelegate.write(collected);
    }

    @Override
    public void open(@NonNull ExecutionContext executionContext) throws ItemStreamException {
        if (itemWriterDelegate instanceof ItemStreamWriter) {
            ((ItemStreamWriter<I>) itemWriterDelegate).open(executionContext);
        }
    }

    @Override
    public void update(@NonNull ExecutionContext executionContext) throws ItemStreamException {
        if (itemWriterDelegate instanceof ItemStreamWriter) {
            ((ItemStreamWriter<I>) itemWriterDelegate).update(executionContext);
        }
    }

    @Override
    public void close() throws ItemStreamException {
        if (itemWriterDelegate instanceof ItemStreamWriter) {
            ((ItemStreamWriter<I>) itemWriterDelegate).close();
        }
    }

}
