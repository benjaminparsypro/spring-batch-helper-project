package com.benjamin.parsy.springbatchhelper;

import java.util.function.Function;

public interface Mapper<I, M> extends Function<I, M> {
}
