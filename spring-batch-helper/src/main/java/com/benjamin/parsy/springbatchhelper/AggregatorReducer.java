package com.benjamin.parsy.springbatchhelper;

import org.springframework.lang.NonNull;

import java.util.List;

@FunctionalInterface
public interface AggregatorReducer<R, A> {

    /**
     * Function to reduce a list of raw items to read item
     *
     * @param rawItemList Raw item list read
     * @return An aggregate of raw items read
     */
    @NonNull
    A reduce(@NonNull List<R> rawItemList);

}
