package com.benjamin.parsy.springbatchhelper;

import java.util.function.Function;

public interface FlatMapper<I, M> extends Function<I, M> {
}
