package com.benjamin.parsy.springbatchhelper;

import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.ItemStreamException;
import org.springframework.batch.item.ItemStreamReader;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.lang.NonNull;
import org.springframework.util.Assert;

import java.util.LinkedList;

public abstract class AggregatorItemReader<R, A> implements ItemStreamReader<A>, InitializingBean {

    private final ItemReader<R> rawItemReader;
    private final AggregatorCompletor<R> aggregatorCompletor;
    private final AggregatorReducer<R, A> aggregatorReducer;

    private R lastItemNotAggregate;

    protected AggregatorItemReader(ItemReader<R> rawItemReader, AggregatorCompletor<R> aggregatorCompletor, AggregatorReducer<R, A> aggregatorReducer) {
        this.rawItemReader = rawItemReader;
        this.aggregatorCompletor = aggregatorCompletor;
        this.aggregatorReducer = aggregatorReducer;
    }

    @Override
    public void afterPropertiesSet() {
        Assert.notNull(this.aggregatorCompletor, "AggregatorCompletor is required");
        Assert.notNull(this.aggregatorCompletor, "AggregatorReducer is required");
        Assert.notNull(this.rawItemReader, "ItemReader is required");
    }

    @Override
    public A read() throws Exception {

        LinkedList<R> currentItemRawList = new LinkedList<>();

        if (lastItemNotAggregate != null) {
            currentItemRawList.addLast(lastItemNotAggregate);
            lastItemNotAggregate = null;
        }

        R currentItemRaw;
        while ((currentItemRaw = rawItemReader.read()) != null) {

            if (!currentItemRawList.isEmpty() && !aggregatorCompletor.toBeAggregated(currentItemRawList.getLast(), currentItemRaw)) {
                lastItemNotAggregate = currentItemRaw;
                return aggregatorReducer.reduce(currentItemRawList);
            }

            currentItemRawList.addLast(currentItemRaw);
        }

        return currentItemRawList.isEmpty() ? null : aggregatorReducer.reduce(currentItemRawList);
    }

    @Override
    public void open(@NonNull ExecutionContext executionContext) throws ItemStreamException {
        if (rawItemReader instanceof ItemStreamReader) {
            ((ItemStreamReader<R>) rawItemReader).open(executionContext);
        }
    }

    @Override
    public void update(@NonNull ExecutionContext executionContext) throws ItemStreamException {
        if (rawItemReader instanceof ItemStreamReader) {
            ((ItemStreamReader<R>) rawItemReader).update(executionContext);
        }
    }

    @Override
    public void close() throws ItemStreamException {
        if (rawItemReader instanceof ItemStreamReader) {
            ((ItemStreamReader<R>) rawItemReader).close();
        }
    }
}