package com.benjamin.parsy.springbatchhelper;

import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.item.ItemStreamException;
import org.springframework.batch.item.ItemStreamWriter;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.lang.NonNull;
import org.springframework.util.Assert;

import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public abstract class FilmapItemWriter<I, M> implements ItemStreamWriter<I>, InitializingBean {

    private final ItemWriter<M> itemWriterDelegate;
    private Mapper<? super I, ? extends M> mapper;
    private FlatMapper<? super I, ? extends Stream<? extends M>> flatMapper;
    private final Predicate<M> predicate;
    private final boolean isFlatMap;

    protected FilmapItemWriter(ItemWriter<M> itemWriterDelegate, Mapper<? super I, ? extends M> mapper, Predicate<M> predicate) {
        this.itemWriterDelegate = itemWriterDelegate;
        this.mapper = mapper;
        this.predicate = predicate;
        this.isFlatMap = false;
    }

    protected FilmapItemWriter(ItemWriter<M> itemWriterDelegate, FlatMapper<? super I, ? extends Stream<? extends M>> flatMapper, Predicate<M> predicate) {
        this.itemWriterDelegate = itemWriterDelegate;
        this.flatMapper = flatMapper;
        this.predicate = predicate;
        this.isFlatMap = true;
    }

    @Override
    public void afterPropertiesSet() {
        Assert.notNull(this.predicate, "Predicate is required");
        Assert.notNull(this.itemWriterDelegate, "ItemWriterDelegate is required");
        if (isFlatMap) {
            Assert.notNull(this.flatMapper, "FlatMapper is required");
        } else {
            Assert.notNull(this.mapper, "Mapper is required");
        }
    }

    @Override
    public void write(@NonNull List<? extends I> list) throws Exception {
        List<M> collected = map(list).filter(predicate).collect(Collectors.toList());
        itemWriterDelegate.write(collected);
    }

    private Stream<M> map(List<? extends I> list) {
        return isFlatMap ? list.stream().flatMap(flatMapper) : list.stream().map(mapper);
    }

    @Override
    public void open(@NonNull ExecutionContext executionContext) throws ItemStreamException {
        if (itemWriterDelegate instanceof ItemStreamWriter) {
            ((ItemStreamWriter<M>) itemWriterDelegate).open(executionContext);
        }
    }

    @Override
    public void update(@NonNull ExecutionContext executionContext) throws ItemStreamException {
        if (itemWriterDelegate instanceof ItemStreamWriter) {
            ((ItemStreamWriter<M>) itemWriterDelegate).update(executionContext);
        }
    }

    @Override
    public void close() throws ItemStreamException {
        if (itemWriterDelegate instanceof ItemStreamWriter) {
            ((ItemStreamWriter<M>) itemWriterDelegate).close();
        }
    }

}
